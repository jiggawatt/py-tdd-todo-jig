Provisioning a new site:

## Req'd Pkgs
* nginx
* Python 3.7
* venv + pip
* Git

## nginx vhost config
* see nginx.template.conf
* replace DOMAIN with domain.example.com

## systemd svc
* see gunicorn-systemd.template.service
* replace DOMAIN with domain.example.com

## folder struct
/home/username/sites/DOMAIN/<.env, db.sqlite3, manage.py, static, virtualenv>

