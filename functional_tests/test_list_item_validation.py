from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys
from unittest import skip



class ItemValidationTest(FunctionalTest):
    
    @skip
    def test_cannot_add_empty_list_items(self):
        # user goes to homepage and accidentally tries to submit an
        # empty list item. Hits enter on empty input box
        self.browser.get(self.live_server_url)
        self.browser.find_element_by_id('id_new_item').send_keys(Keys.ENTER)

        #Homepage refreshes, and there is an error msg saying list
        # cannot be blank
        self.wait_for(lambda: self.assertEqual(
            self.browser.find_element_by_css_selector('.has.error').text, 
            "You can't have an empty list item"
        ))
        #Tries again with some text for item, which now works
        self.browser.find_element_by_id('id_new_item').send_keys('Buy milk')
        self.browser.find_element_by_id('id_new_item').send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy milk')

        #now decides to submit a second blank list item
        self.browser.find_element_by_id('id_new_item').send_keys(Keys.ENTER)

        #receives a similar warning
        self.wait_for(lambda: self.assertEqual(
            self.browser.find_element_by_css_selector('.has.error').text, 
            "You can't have an empty list item"
        ))

        #can correct it by filling some text in
        self.browser.find_element_by_id('id_new_item').send_keys('Make tea')
        self.browser.find_element_by_id('id_new_item').send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy milk')
        self.wait_for_row_in_list_table('2: Make tea')