from .base import FunctionalTest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys



class NewVisitorTest(FunctionalTest):

    def test_can_start_list_for_one_user(self):
        # user checks out to-do app homepage
        self.browser.get(self.live_server_url)

        # user notes page title and header says 'to-do'
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # user invited to enter to-do item
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )

        # user types to-do item in textbox "Buy peacock feathers"
        inputbox.send_keys('Buy peacock feathers')

        # user hits enter, page updates, now page lists
        # "1: Buy peacock feathers" as a list item
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy peacock feathers')

        # there still remains listbox to enter another item
        # enters "Use peacock feathers to make a fly"
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Use peacock feathers to make a fly')
        inputbox.send_keys(Keys.ENTER)

        # page updates again, now shows two items in list
        self.wait_for_row_in_list_table('1: Buy peacock feathers')
        self.wait_for_row_in_list_table(
            '2: Use peacock feathers to make a fly')
        # satisfied, goes backto sleep

    def test_multiple_users_can_start_lists_at_different_urls(self):
        # user starts a new to-do list
        self.browser.get(self.live_server_url)
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Buy peacock feathers')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy peacock feathers')
        # user wonders if the page will remember their list,

        # sees a unique URL has been generated, with explanation to that effect
        user1_list_url = self.browser.current_url
        self.assertRegex(user1_list_url, '/lists/.+')

        # now a new user, user2, visits site
        # we use new browser session to ensure info not leaking via cookies
        self.browser.quit()
        self.browser = webdriver.Firefox()

        # user2 visits homepage, there is no sign of user1's list
        self.browser.get(self.live_server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy peacock feathers', page_text)
        self.assertNotIn('make a fly', page_text)

        # user2 starts new list by entering a new item
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Buy milk')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy milk')

        # user2 gets his own unique URL
        user2_list_url = self.browser.current_url
        self.assertRegex(user2_list_url, '/lists/.+')
        self.assertNotEqual(user2_list_url, user1_list_url)

        # again, there is no trace of user1's list
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy peacock feathers', page_text)
        self.assertIn('Buy milk', page_text)

        # satisfied, both go back to sleep
